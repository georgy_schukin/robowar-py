from robowar.server.engine.game.game import Game
from robowar.client.gl.vis_listener import VisListener
import robowar.client.gl.client as client

import multiprocessing

def run_game(listeners):
    scripts = [("simple", "robowar/bots/simple_bot.py")]
    teams = [("team1", "simple"), ("team2", "simple")]

    game = Game.generate_test_game(map_width=50,
                                   map_height=50,
                                   scripts=scripts,
                                   teams=teams,
                                   bases_per_team=5)

    for l in listeners:
        game.add_listener(l)

    game.run()

def run_client(connection):
    client.run(connection)

def main():
    receiving_conn, sending_conn = multiprocessing.Pipe(duplex=False)
    client_process = multiprocessing.Process(target=run_client, args=(receiving_conn,))
    client_process.start()
    receiving_conn.close()
    run_game([VisListener(sending_conn)])

if __name__ == "__main__":
    main()
