import socket

from robowar.common.message.message_sender import MessageSender
from robowar.common.coords import HexCoord
from robowar.server.engine.model.robot import RobotFactory, Robot
from robowar.server.engine.model.map import Map

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("localhost", 12345))

sender = MessageSender(sock)

factory = RobotFactory()
robot = factory.create_robot()
robot.coord = HexCoord(2, 3)

sender.send("new game", {"a": 3, "b": HexCoord(1, 3).to_json()})
sender.send("new game", robot)

robo_json = robot.to_json()
robot2 = Robot.from_json(robo_json)
print(robot2.coord)

mp = Map.make_rectangle_map(10, 10)
mp_json = mp.to_json()
mp2 = Map.from_json(mp_json)
print(mp2.all_coords(), mp2.id)

sock.close()
