from robowar.server.server import Server
from robowar.server.server_config import ServerConfig

def main():
    server_config = ServerConfig()
    server_config.parse_args()
    server = Server(server_config)
    server.run()

if __name__ == "__main__":
    main()
