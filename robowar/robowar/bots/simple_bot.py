import random

def execute(rc):
    while not rc.is_moving():
        rc.move_in_direction(random.randint(0, 6))
    for robot in rc.robots_in_view:
        if rc.is_enemy(robot):
            rc.shoot_at(robot.coord)
            if rc.is_shooting():
                break
