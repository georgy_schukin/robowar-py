from .visualizer import Visualizer

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

visualizer = None

def init_gl():
    glClearColor(0.2, 0.2, 0.3, 1.0)

def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    if visualizer:
        visualizer.draw()

    glutSwapBuffers()

def set_ortho2d(area):
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(area[0], area[1], area[2], area[3])
    glMatrixMode(GL_MODELVIEW)

def reshape(width, height):
    glViewport(0, 0, width, height)

def keyboard(key, x, y):
    if key == '\033':
        sys.exit()

def update():
    if visualizer:
        glutSetWindowTitle("RoboWar client (frame {0})".format(visualizer.frame))
        set_ortho2d(visualizer.map_area)
        glutPostRedisplay()

def run(connection, title="RoboWar client"):
    glutInit()
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH)
    glutCreateWindow(title.encode())
    glutReshapeWindow(512, 512)
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutKeyboardFunc(keyboard)
    glutIdleFunc(update)

    init_gl()

    global visualizer
    visualizer = Visualizer(connection)

    glutMainLoop()


