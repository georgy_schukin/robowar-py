from robowar.common.jsonable import JSONable


class MapObject(object):

    def __init__(self, values):
        super().__init__()
        self.coord = JSONable.value_from_json(values["coord"])
        self.team_id = values["team"]["id"]


class Robot(MapObject):

    def __init__(self, values):
        super().__init__(values)
        self.shoot_coord = JSONable.value_from_json(values["shoot_coord"])


class Base(MapObject):

    def __init__(self, values):
        super().__init__(values)

