from robowar.server.engine.game.game_listener import GameListener
from robowar.common.jsonable import JSONable

import threading
import queue

class VisListener(GameListener):

    def __init__(self, connection):
        super().__init__()
        self.connection = connection
        self.send_queue = queue.Queue()
        self.send_thread = threading.Thread(target=self.send_updates)
        self.send_thread.daemon = True
        self.send_thread.start()

    def send_updates(self):
        while True:
            data = self.send_queue.get()
            try:
                self.connection.send(data)
            except ValueError:
                print("Failed to send update!")

    def send_init(self, teams, map_coords):
        data = {"tag": "init",
                "teams": JSONable.value_to_json(teams),
                "coords": JSONable.value_to_json(map_coords)}
        self.send_queue.put(data)

    def send_update(self, frame_number, robots, bases):
        data = {"tag": "update",
                "robots": JSONable.value_to_json(robots),
                "bases": JSONable.value_to_json(bases),
                "frame": frame_number}
        self.send_queue.put(data)
