import robowar.client.gl.objects as objects
from robowar.common.jsonable import JSONable

from OpenGL.GL import *
import threading

class Visualizer(object):

    hex_size = 1.0

    def __init__(self, connection):
        super().__init__()
        self.connection = connection
        self.teams = []
        self.team_colors = {}
        self.px_coords = {}
        self.map_area = [-10, 10, -10, 10]
        self.robots = []
        self.bases = []
        self.frame = None
        self.update_thread = threading.Thread(target=self.receive_updates)
        self.update_thread.daemon = True
        self.update_thread.start()

    def __del__(self):
        self.connection.close()

    def receive_updates(self):
        while True:
            try:
                data = self.connection.recv()
            except EOFError:
                return
            else:
                if data is None:
                    return
                else:
                    self.update_from_json(data)

    def update_from_json(self, json_obj):
        if json_obj["tag"] == "init":
            self.teams = JSONable.value_from_json(json_obj["teams"])
            self.team_colors = Visualizer._get_team_colors(self.teams)
            coords = JSONable.value_from_json(json_obj["coords"])
            self.px_coords = Visualizer._get_px_coords(coords)
            self.map_area = Visualizer._get_map_area(coords)
        elif json_obj["tag"] == "update":
            self.robots = [objects.Robot(r) for r in json_obj["robots"]]
            self.bases = [objects.Base(b) for b in json_obj["bases"]]
            self.frame = json_obj["frame"]

    def draw(self):
        self._draw_map()
        self._draw_robots()
        self._draw_bases()
        self._draw_shootings()

    def _draw_map(self):
        glPointSize(1)
        glColor3f(0.6, 0.6, 0.6)
        glBegin(GL_POINTS)
        for coord in self.px_coords.values():
            glVertex2fv(coord)
        glEnd()

    def _draw_robots(self):
        glPointSize(5)
        self._draw_objects(self.robots)

    def _draw_bases(self):
        glPointSize(10)
        self._draw_objects(self.bases)

    def _draw_shootings(self):
        glBegin(GL_LINES)
        for robot in self.robots:
            if robot.shoot_coord is not None:
                glColor3f(1.0, 1.0, 1.0)
                glVertex2fv(self._px(robot.coord))
                glColor3f(0.4, 0.4, 0.4)
                glVertex2fv(self._px(robot.shoot_coord))
        glEnd()

    def _draw_objects(self, objects):
        glBegin(GL_POINTS)
        for obj in objects:
            glColor3fv(self.team_colors[obj.team_id])
            glVertex2fv(self._px(obj.coord))
        glEnd()

    @staticmethod
    def _get_map_area(coords):
        pix_coords = [c.to_pixel(Visualizer.hex_size) for c in coords]
        min_x = min([p[0] for p in pix_coords]) - 2*Visualizer.hex_size
        max_x = max([p[0] for p in pix_coords]) + 2*Visualizer.hex_size
        min_y = min([p[1] for p in pix_coords]) - 2*Visualizer.hex_size
        max_y = max([p[1] for p in pix_coords]) + 2*Visualizer.hex_size
        return [min_x, max_x, min_y, max_y]

    @staticmethod
    def _get_px_coords(coords):
        """Get pixel coords for cell coords"""
        return {c: c.to_pixel(Visualizer.hex_size) for c in coords}

    @staticmethod
    def _get_team_colors(teams):
        colors = [[1.0, 0.0, 0.0],
                  [0.0, 1.0, 0.0],
                  [0.0, 0.0, 1.0]]
        return {teams[i].id: colors[i % len(colors)] for i in range(len(teams))}

    def _px(self, coord):
        """Get pixel coord for cell coord"""
        return self.px_coords[coord]



