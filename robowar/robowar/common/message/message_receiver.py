import json

class MessageReceiver(object):
    """Receives messages from a socket.
    None message indicates closed connection."""

    size_delim = ":"
    receive_size = 4096
    
    def __init__(self, socket):                  
        self.socket = socket
        self._data_received = ""

    def receive(self, from_json=True):
        return self._receive_json() if from_json else self._receive_string()

    def _receive_json(self):
        """Receive message as JSON document"""
        message = self._receive_string()
        if message is None:
            return None
        try:
            json_doc = json.loads(message)
        except ValueError as e:
            print("MessageReceiver error: not a valid JSON")
            print(e)
            return {}
        else:
            return json_doc

    def _receive_string(self):
        """Receive message as a string"""
        size = self._get_message_size()
        if size is None:
            return None                                
        return self._get_message(size)                        

    def _get_message_size(self):
        while MessageReceiver.size_delim not in self._data_received:
            data = self._receive_data()
            if not data:
                return None  # connection is closed
            self._data_received += data.decode()
        size_str, sep, self._data_received = self._data_received.partition(MessageReceiver.size_delim)
        try:
            size = int(size_str)
        except (ValueError, TypeError):
            print("Error: received bad message size: {0}".format(size_str))
            return 0
        else:
            return size

    def _get_message(self, size):        
        if size <= 0:
            return ""
        while len(self._data_received) < size:
            data = self._receive_data()
            if not data:
                return None  # connection is closed
            self._data_received += data.decode()
        message, self._data_received = self._data_received[:size], self._data_received[size:]
        return message               

    def _receive_data(self):
        return self.socket.recv(MessageReceiver.receive_size)
