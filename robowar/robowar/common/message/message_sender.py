from robowar.common.jsonable import JSONable

import socket
import json

class MessageSender(object):
    """Sends message into socket."""

    size_delim = ":"

    def __init__(self, dest_socket):
        self.socket = dest_socket

    def send(self, tag, data="", to_json=True):
        if to_json:
            self._send_json(tag, data)
        else:
            self._send_string(tag + data)

    def _send_json(self, tag, data):
        """Convert message to JSON"""
        try:
            json_data = data.to_json() if isinstance(data, JSONable) else data
            message = {"tag": tag, "data": json_data}
            json_str = json.dumps(message)
        except (TypeError, OverflowError, ValueError) as e:
            print("MessageSender error: failed to convert to JSON!")
            print(e)
        else:
            self._send_string(json_str)

    def _send_string(self, message):
        """Send string message"""
        to_send = "{0}{1}{2}".format(len(message), MessageSender.size_delim, message)
        try:
            self.socket.sendall(to_send.encode())
        except socket.error as e:
            print("Socket error!", e)

