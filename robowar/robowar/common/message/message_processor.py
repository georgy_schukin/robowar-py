class MessageProcessor(object):
    """Processes received messages."""

    def __init__(self):
        super().__init__()

    def process(self, message):
        """Message is assumed to be a JSON object"""
        try:
            tag = message.get("tag")
            data = message.get("data")
            self._process_message(tag, data)
        except (ValueError, TypeError) as e:
            print("Message processing error:", e)

    def _process_message(self, tag, data):
        if tag is None:
            print("No message tag is found!")
            return
        handler = self._get_handler(tag)
        handler(tag, data)

    def on_unknown(self, tag, data):
        print("Unknown message tag: \"{0}\" (data: \"{1}\")".format(tag, data))

    def _get_handler(self, tag):
        handler_name = MessageProcessor._get_handler_name(tag)
        handler = getattr(self, handler_name, None)
        return handler if callable(handler) else self.on_unknown

    @staticmethod
    def _get_handler_name(tag):
        return "on_" + "_".join(tag.lower().split())
