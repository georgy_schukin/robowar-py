import importlib

class ClassLibrary:

    def __init__(self):
        self.modules = {}

    def get_class(self, obj_module, obj_class):
        if obj_module not in self.modules:
            self.modules[obj_module] = importlib.import_module(obj_module)
        return getattr(self.modules[obj_module], obj_class)
