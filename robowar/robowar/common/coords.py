from robowar.common.jsonable import JSONable

import math

class CubeCoord(object):
    """Cube coordinates for a hex grid"""
    
    def __init__(self, x, y, z):
        super().__init__()        
        self.coords = [x, y, z]        

    def to_hex(self):
        return HexCoord(self.coords[0], self.coords[2])

    def add(self, cube_delta):
        cc = [self.coords[i] + cube_delta.coords[i] for i in range(3)]
        return CubeCoord(cc[0], cc[1], cc[2])

    def distance_to(self, cc):
        deltas = [abs(self.coords[i] - cc.coords[i]) for i in range(3)]
        return max(deltas)

    def coords_in_range(self, distance):
        coords = []        
        for dx in range(-distance, distance + 1):
            for dy in range(max(-distance, -dx - distance), min(distance, -dx + distance) + 1):
                dz = -dx - dy
                coords.append(self.add(CubeCoord(dx, dy, dz)))                            
        return coords

class HexCoord(JSONable):
    """Axial coordinates for a hex grid"""

    def __init__(self, q=None, r=None):
        super().__init__()        
        self.q = q
        self.r = r

    def __eq__(self, other):
        return isinstance(other, HexCoord) and ((self.q, self.r) == (other.q, other.r))

    def __hash__(self):
        return hash((self.q, self.r))

    def __repr__(self):
        return "({0}, {1})".format(self.q, self.r)

    def x(self):
        return self.q

    def y(self):
        return self.r

    def to_cube(self):
        return CubeCoord(self.q, -self.q - self.r, self.r)

    def add(self, hex_delta):
        return HexCoord(self.q + hex_delta.q, self.r + hex_delta.r)

    @staticmethod
    def direction(dir_index):
        return _hex_directions[dir_index]

    def neighbour(self, dir_index):        
        return self.add(HexCoord.direction(dir_index))

    def neighbours(self, include_me=False):
        dir_indices = range(7) if include_me else range(1, 7)
        return [self.neighbour(i) for i in dir_indices]

    def distance_to(self, hc):
        cube_coord = self.to_cube()
        return cube_coord.distance_to(hc.to_cube())

    def coords_in_range(self, distance):
        cube_coords = self.to_cube().coords_in_range(distance)
        return [cc.to_hex() for cc in cube_coords]

    def to_json(self):
        return self._to_json_object({"x": self.q, "y": self.r})

    @classmethod
    def from_json(cls, json_obj):
        return HexCoord(json_obj["x"], json_obj["y"])

    def to_pixel(self, size=1.0):
        px = size * math.sqrt(3) * (self.q + self.r/2)
        py = size * 3/2 * self.r
        return px, py

_hex_directions = [HexCoord(0, 0),
                   HexCoord(1, 0), HexCoord(1, -1), HexCoord(0, -1),
                   HexCoord(-1, 0), HexCoord(-1, 1), HexCoord(0, 1)]
