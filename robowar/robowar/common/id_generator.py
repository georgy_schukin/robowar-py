class IdGenerator(object):

    def __init__(self):
        super().__init__()
        self.current_id = 0

    def next_id(self):
        new_id = self.current_id
        self.current_id += 1
        return new_id
