from .class_library import ClassLibrary

class JSONable(object):
    """Object that can be converted to JSON representation"""

    class_tag = "__class__"
    module_tag = "__module__"
    class_library = ClassLibrary()

    def __init__(self):
        super().__init__()

    def to_json(self):
        """Convert to JSON (takes all public variables by default)"""
        public_vars = self._get_public_variables()
        return self._to_json_object(public_vars)

    def _to_json_object(self, values):
        return JSONable.value_to_json(self._plus_metatags(values))

    @classmethod
    def from_json(cls, json_obj):
        """Creates object from JSON"""
        new_object = cls()  # create new object here
        if isinstance(json_obj, dict):
            for (key, value) in json_obj.items():
                if not key.startswith("_"):
                    setattr(new_object, key, cls._from_json_object(value))
        return new_object

    @classmethod
    def _from_json_object(cls, json_obj):
        return cls.value_from_json(json_obj)

    def _get_public_variables(self):
        return {key: value for (key, value) in vars(self).items() if not key.startswith("_")}  # get all public vars

    def _plus_metatags(self, json_obj):
        if isinstance(json_obj, dict):
            json_obj[JSONable.class_tag] = type(self).__name__
            json_obj[JSONable.module_tag] = type(self).__module__
        return json_obj

    @staticmethod
    def value_to_json(value):
        if isinstance(value, dict):
            return {k: JSONable.value_to_json(v) for (k, v) in value.items()}  # serialize dict
        elif isinstance(value, list):
            return [JSONable.value_to_json(v) for v in value]  # serialize list
        elif isinstance(value, JSONable):
            return value.to_json()  # serialize JSONable object
        else:
            return value  # serialize scalar value

    @staticmethod
    def value_from_json(value):
        if isinstance(value, dict):
            obj = JSONable._get_object(value)
            if obj:
                return obj  # deserialize JSONable object
            else:
                return {k: JSONable.value_from_json(v) for (k, v) in value.items()}  # deserialize dict
        elif isinstance(value, list):
            return [JSONable.value_from_json(v) for v in value]  # deserialize list
        else:
            return value

    @staticmethod
    def _get_object(value):
        module_name = value.get(JSONable.module_tag)
        class_name = value.get(JSONable.class_tag)
        if module_name is not None and class_name is not None:
            obj_class = JSONable.class_library.get_class(module_name, class_name)
            if obj_class:
                return obj_class.from_json(value) if issubclass(obj_class, JSONable) else obj_class()
        return None


