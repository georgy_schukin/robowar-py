import socket

from robowar.server.client_thread import ClientThread
from robowar.server.server_message_processor import ServerMessageProcessor

class Server(object):
    """RoboWar server - processes requests from clients"""

    def __init__(self, server_config):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.config = server_config

    def run(self):
        if not self._start_listen():
            return
        while True:
            client_socket, address = self.socket.accept()
            print("Connected from", address)
            client_thread = ClientThread(client_socket, ServerMessageProcessor())
            client_thread.start()
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def _start_listen(self):
        try:
            self.socket.bind(("", self.config.port))
        except socket.error as msg:
            print("Bind error:", msg)
            return False
        else:
            self.socket.listen(self.config.max_connections)
            print("Start listening...")
            return True
