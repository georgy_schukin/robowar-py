import argparse

class ServerConfig(object):
    """Configuration for RoboWar server"""

    port_default = 12345
    max_connections_default = 10
    
    def __init__(self):
        self.port = ServerConfig.port_default
        self.max_connections = ServerConfig.max_connections_default

    def parse_args(self):
        parser = argparse.ArgumentParser(description="RoboWar server configuration")
        parser.add_argument("-p", "--port", type=int, default=ServerConfig.port_default,
                            help="Port for the server to listen on")
        parser.add_argument("--max-connections", type=int, default=ServerConfig.max_connections_default,
                            help="Maximum number of client connections")
        
        args = parser.parse_args()

        self.port = args.port
        self.max_connections = args.max_connections
