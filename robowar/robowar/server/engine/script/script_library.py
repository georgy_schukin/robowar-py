from robowar.server.engine.script.robot_script import RobotScript

import os
import random
import shutil

class ScriptLibrary(object):
    script_directory_base = "robot_scripts"

    def __init__(self):
        super().__init__()
        #self.script_directory = "{0}-{1}".format(ScriptLibrary.script_directory_base, random.randint(0, 100000))
        self.script_directory = ScriptLibrary.script_directory_base
        self.script_directory_path = os.path.join(os.getcwd(), self.script_directory)
        self._scripts = {}
        ScriptLibrary._create_module_dir(self.script_directory_path)

    def __del__(self):
        ScriptLibrary._delete_module_dir(self.script_directory_path)

    @staticmethod
    def _create_module_dir(dir_path):
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        module_tag_file = os.path.join(dir_path, "__init__.py")
        if not os.path.exists(module_tag_file):
            open(module_tag_file, "a+").close()

    @staticmethod
    def _delete_module_dir(dir_path):
        if os.path.exists(dir_path):
            shutil.rmtree(dir_path)

    def add_script(self, name, script):
        if os.path.isfile(script):
            self.add_script_from_file(name, script)
        else:
            self.add_script_from_contents(name, script)

    def get_script(self, name):
        return self._scripts.get(name)

    def add_script_from_contents(self, name, contents):
        if self.get_script(name) is None:
            self._scripts[name] = RobotScript(name, self.script_directory, self.script_directory_path, contents)

    def add_script_from_file(self, name, file_name):
        if os.path.exists(file_name) and os.path.isfile(file_name):
            with open(file_name, "r") as f:
                contents = f.read()
                self.add_script_from_contents(name, contents)

