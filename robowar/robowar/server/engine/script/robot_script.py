import importlib
import os
import re

class RobotScript(object):
    main_function = "execute"

    def __init__(self, name, base_module, base_module_path, contents):
        super().__init__()
        self.name = RobotScript._make_module_name(name)
        self._base_module = base_module
        self._base_module_path = base_module_path
        self._module = self._create_module(contents)
        self._function = RobotScript._get_main_function(self._module)

    @staticmethod
    def _make_module_name(name):
        return re.sub(r"\W*", "", name.lower())

    def _create_module(self, contents):
        self._create_module_file(contents)
        return self._import_module()

    def _create_module_file(self, contents):
        module_file = os.path.join(self._base_module_path, self.name + ".py")
        with open(module_file, "w") as f:
            f.write(contents)

    def _import_module(self):
        module_name = "{0}.{1}".format(self._base_module, self.name)
        return importlib.import_module(module_name)

    @staticmethod
    def _get_main_function(module):
        return getattr(module, RobotScript.main_function)

    def run(self, robot_controller):
        if self._function:
            self._function(robot_controller)
