from robowar.server.engine.model.game_object import GameObject

class MapObject(GameObject):
    """Object that can be placed on a map and belong to a team."""
    
    def __init__(self, coord=None, team=None):
        super().__init__()
        self.coord = coord  # coord on the map
        self.team = team  # team to which object belongs

    def distance_to(self, coord):
        return self.coord.distance_to(coord)

    def is_enemy(self, obj):
        return self.team.id != obj.team.id
