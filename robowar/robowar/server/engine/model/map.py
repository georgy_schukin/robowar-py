from robowar.server.engine.model.game_object import GameObject
from robowar.server.engine.model.cell import Cell
from robowar.common.coords import HexCoord

import math
import random

class Map(GameObject):
    
    def __init__(self):
        super().__init__()        
        self._cells = {}    

    def add_cell(self, coord):
        cell = Cell(coord)
        self._cells[coord] = cell
        return cell

    def cell(self, coord):
        """Return cell for coord or None if there is no such"""
        return self._cells.get(coord)

    def cells(self, coords):
        """Return existing cells for coords"""
        cells = [self.cell(c) for c in coords]
        return [cell for cell in cells if cell is not None]

    def has_cell(self, coord):
        return coord in self._cells

    def neighbour_cell(self, coord, dir_index):
        return self.cell(coord.neighbour(dir_index))

    def neighbour_cells(self, coord, include_center=False):
        coords = coord.neighbours(include_center)
        return self.cells(coords)

    def cells_in_range(self, center_coord, distance, include_center=False):
        coords = center_coord.coords_in_range(distance)
        if not include_center:
            coords.remove(center_coord)
        return self.cells(coords)
        
    @staticmethod
    def make_rectangle_map(width, height):
        game_map = Map()        
        for r in range(height):
            r_offset = math.floor(r/2)    
            for q in range(-r_offset, width - r_offset):    
                game_map.add_cell(HexCoord(q, r))
        return game_map

    def add_object(self, obj, coord):
        cell = self.cell(coord)
        if cell is not None:
            obj.coord = coord
            cell.add_object(obj)

    def move_object(self, obj, new_coord):
        self.remove_object(obj)  # remove from previous cell if any
        self.add_object(obj, new_coord)

    def remove_object(self, obj):
        cell = self.cell(obj.coord)
        if cell is not None:
            cell.remove_object(obj)

    def object_on_coord(self, obj_type, coord):
        cell = self.cell(coord)
        return cell.get_object(obj_type) if cell else None

    def random_cell(self):
        keys = list(self._cells.keys())
        return self._cells[keys[random.randint(0, len(keys) - 1)]]

    def clear(self):
        for cell in self._cells:
            cell.clear()

    def all_coords(self):
        return list(self._cells.keys())

    def to_json(self):
        data = super().to_json()
        data["_coords"] = self.all_coords()
        return self._to_json_object(data)

    @classmethod
    def from_json(cls, json_obj):
        mp = super().from_json(json_obj)
        coords = cls._from_json_object(json_obj["_coords"])
        for coord in coords:
            mp.add_cell(coord)
        return mp

