from robowar.server.engine.model.game_object import GameObject

class Team(GameObject):
    
    def __init__(self, name=None, script_name=None):
        super().__init__()
        self.name = name
        self.script_name = script_name

