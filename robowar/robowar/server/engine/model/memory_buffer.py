class MemoryBuffer(object):

    def __init__(self, size):
        super().__init__()
        self._size = size
        self._buffer = [0]*size

    def set(self, index, value):
        if 0 <= index < self._size:
            self._buffer[index] = value

    def get(self, index):
        return self._buffer[index] if 0 <= index < self._size else 0

    def size(self):
        return self._size
