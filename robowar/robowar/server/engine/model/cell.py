from robowar.server.engine.model.game_object import GameObject

class Cell(GameObject):
    
    def __init__(self, coord=None):
        super().__init__()
        self.coord = coord        
        self._objects = []

    def add_object(self, obj):
        self._objects.append(obj)

    def get_object(self, obj_type):
        return next((obj for obj in self._objects if isinstance(obj, obj_type)), None)

    def remove_object(self, obj):
        if obj in self._objects:
            self._objects.remove(obj)

    def clear(self):
        self._objects.clear()

    def empty(self):
        return len(self._objects) == 0

    def contains(self, obj_type):
        return self.get_object(obj_type) is not None
