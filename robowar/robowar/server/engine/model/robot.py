from robowar.server.engine.model.map_object import MapObject
from robowar.server.engine.model.memory_buffer import MemoryBuffer

class RobotFactory(object):

    def __init__(self, config=None):
        super().__init__()
        self.config = config if config else RobotFactory.default_config()

    @staticmethod
    def default_config():
        config = {"hit_points": 5,
                  "view_range": 5,
                  "shoot_range": 5,
                  "transmit_range": 10,
                  "memory_size": 128,
                  "message_size": 32,
                  "weapon_power": 1,
                  "weapon_cooldown": 5
                  }
        return config

    def create_robot(self, team=None):
        return Robot(self.config, team=team)


class Robot(MapObject):
    
    def __init__(self, config=RobotFactory.default_config(), team=None):
        super().__init__(team=team)
        for key, value in config.items():
            setattr(self, key, value)
        self.weapon_reload = 0
        self.is_transmitting = False
        self.shoot_coord = None
        self.move_coord = None
        self._memory = MemoryBuffer(self.memory_size)
        self._transmit_message = MemoryBuffer(self.message_size)

    def reset(self):
        self.cancel_move()
        self.cancel_shoot()

    def cancel_move(self):
        self.move_coord = None

    def cancel_shoot(self):
        self.shoot_coord = None

    def receive_damage(self, damage):
        self.hit_points -= damage

    def receive_damage_from(self, robot):
        self.receive_damage(robot.weapon_power)

    def update(self):
        """Update for next iteration"""
        if self.weapon_reload > 0:
            self.weapon_reload -= 1
        elif self.shooted():
            self.weapon_reload = self.weapon_cooldown

    def can_shoot(self):
        return self.weapon_reload == 0

    def is_dead(self):
        return self.hit_points <= 0

    def shooted(self):
        return self.shoot_coord is not None

    def next_coord(self):
        return self.move_coord if self.move_coord else self.coord

    def can_transmit_to(self, robot):
        return self.is_transmitting and self.distance_to(robot.coord) <= self.transmit_range

    def transmission(self):
        return self._transmit_message

    def memory(self):
        return self._memory
