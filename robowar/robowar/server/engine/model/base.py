from robowar.server.engine.model.map_object import MapObject

class BaseFactory(object):

    def __init__(self, config=None):
        super().__init__()
        self.config = config if config else BaseFactory.default_config()

    @staticmethod
    def default_config():
        config = {"hit_points": 10,
                  "construction_time": 5
                  }
        return config

    def create_base(self, team=None):
        return Base(self.config, team=team)


class Base(MapObject):
    
    def __init__(self, config=BaseFactory.default_config(), team=None):
        super().__init__(team=team)
        for key, value in config.items():
            setattr(self, key, value)
        self.construction_reload = 0

    def ready_to_spawn(self):
        return self.is_active() and self.construction_reload == 0

    def is_active(self):
        return self.team is not None

    def update(self):
        if self.is_active():
            if self.construction_reload == 0:
                self.construction_reload = self.construction_time
            else:
                self.construction_reload -= 1
