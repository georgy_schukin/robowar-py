from robowar.common.id_generator import IdGenerator
from robowar.common.jsonable import JSONable

class GameObject(JSONable):
    _id_generator = IdGenerator()

    def __init__(self):
        super().__init__()
        self.id = GameObject._id_generator.next_id()

