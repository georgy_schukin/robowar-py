class GameListener(object):

    def __init__(self):
        super().__init__()

    def send_init(self, teams, map_coords):
        raise NotImplemented("Override this!")

    def send_update(self, frame_number, robots, bases):
        raise NotImplemented("Override this!")
