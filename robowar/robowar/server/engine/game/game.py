from robowar.server.engine.control.game_controller import GameController
from robowar.server.engine.model.map import Map

class Game:

    def __init__(self, game_controller):
        self.game = game_controller

    def add_listener(self, listener):
        self.game.add_listener(listener)

    def run(self):
        self.game.init_iterations()
        while not self.game.is_finished():
            print("Doing frame", self.game.current_frame())
            self.game.iterate()

    @staticmethod
    def generate_test_game(map_width, map_height, scripts, teams, bases_per_team):
        game_map = Map.make_rectangle_map(map_width, map_height)
        gc = GameController(game_map)
        for (name, script) in scripts:
            gc.add_robot_script(name, script)
        for (name, script_name) in teams:
            gc.add_team(name, script_name)
        Game._place_bases(gc, bases_per_team)
        return Game(gc)

    @staticmethod
    def _place_bases(gc, bases_per_team):
        for team in gc.teams:
            for i in range(bases_per_team):
                Game._place_base(gc, team)

    @staticmethod
    def _place_base(gc, team):
        game_map = gc.world.map
        num_of_tries = 0
        while True:
            cell = game_map.random_cell()
            max_dist = max([b.distance_to(cell.coord) for b in gc.world.bases], default=0)
            if max_dist >= 5 or num_of_tries > 10:
                gc.add_base(team, cell.coord)
                return
            else:
                num_of_tries += 1

