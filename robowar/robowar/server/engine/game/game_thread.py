import threading

class GameThread(threading.Thread):

    def __init__(self, game):
        super().__init__()
        self.game = game

    def run(self):
        self.game.run()

