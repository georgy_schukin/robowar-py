from robowar.server.engine.model.robot import Robot
from robowar.server.engine.model.base import Base

class WorldController(object):
    
    def __init__(self, game_map):
        super().__init__()
        self.map = game_map
        self.robots = []
        self.bases = []

    def add_robot(self, robot, coord):
        self.robots.append(robot)
        self.map.add_object(robot, coord)

    def move_robot(self, robot, new_coord):
        self.map.move_object(robot, new_coord)

    def remove_robot(self, robot):
        if robot in self.robots:
            self.map.remove_object(robot)
            self.robots.remove(robot)

    def replace_robot(self, robot, new_robot):
        coord = robot.coord
        self.remove_robot(robot)
        self.add_robot(new_robot, coord)

    def robot_on_coord(self, coord):
        return self.map.object_on_coord(Robot, coord)

    def remove_robots(self, robots):
        for robot in robots:
            self.remove_robot(robot)

    def add_base(self, base, coord):
        self.bases.append(base)
        self.map.add_object(base, coord)

    def remove_base(self, base):
        if base in self.bases:
            self.map.remove_object(base)
            self.bases.remove(base)

    def base_on_coord(self, coord):
        return self.map.object_on_coord(Base, coord)

    def remove_all_robots(self):
        for robot in self.robots:
            self.map.remove_object(robot)
        self.robots.clear()

    def remove_all_bases(self):
        for base in self.bases:
            self.map.remove_object(base)
        self.bases.clear()

    def clear(self):
        self.remove_all_robots()
        self.remove_all_bases()
