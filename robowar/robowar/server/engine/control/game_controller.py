from robowar.server.engine.control.world_controller import WorldController
from robowar.server.engine.control.robot_controller import RobotController
from robowar.server.engine.script.script_library import ScriptLibrary
from robowar.server.engine.model.team import Team
from robowar.server.engine.model.robot import RobotFactory
from robowar.server.engine.model.base import BaseFactory

class GameController(object):

    def __init__(self, game_map):
        super().__init__()
        self.world = WorldController(game_map)
        self.teams = []
        self._scripts = ScriptLibrary()
        self._robot_factory = RobotFactory()
        self._base_factory = BaseFactory()
        self._listeners = []
        self._current_frame = 0

    def add_listener(self, listener):
        self._listeners.append(listener)

    def add_robot_script(self, name, script):
        self._scripts.add_script(name, script)

    def add_team(self, name, script_name):
        team = Team(name, script_name)
        self.teams.append(team)
        return team

    def add_base(self, team, coord):
        base = self._base_factory.create_base(team)
        self.world.add_base(base, coord)

    def init_iterations(self):
        self._send_init()

    def iterate(self):
        self._prepare_iteration()
        self._do_iteration()
        self._post_iteration()

    def _prepare_iteration(self):
        self._remove_dead_robots()
        self._spawn_new_robots()

    def _do_iteration(self):
        robot_controllers = [RobotController(robot, self.world.map) for robot in self.world.robots]
        for rc in robot_controllers:
            script = self._scripts.get_script(rc.team().script_name)
            if script is not None:
                rc.reset()
                script.run(rc)

    def _post_iteration(self):
        self._move_robots()  # move first!
        self._shoot_robots()

        self._send_update()  # send update to client(s)

        self._update_robots()
        self._update_bases()
        self._current_frame += 1

    def is_finished(self):
        return False

    def _update_robots(self):
        self._move_robots()
        for robot in self.world.robots:
            robot.update()

    def _move_robots(self):
        movements = {}
        for robot in self.world.robots:
            if robot.is_dead():
                continue
            coord = robot.next_coord()
            contenders = movements.get(coord, [])
            movements[coord] = contenders + [robot]
        for coord, robots in movements.items():
            if len(robots) == 1:
                self.world.move_robot(robots[0], coord)
            else:
                for robot in robots:
                    robot.cancel_move()

    def _shoot_robots(self):
        for robot in self.world.robots:
            if robot.shooted():
                other_robot = self.world.robot_on_coord(robot.shoot_coord)
                if other_robot:
                    other_robot.receive_damage_from(robot)

    def _update_bases(self):
        for base in self.world.bases:
            base.update()

    def _spawn_new_robots(self):
        spawning_bases = [base for base in self.world.bases if base.ready_to_spawn()]
        for base in spawning_bases:
            self._spawn_new_robot(base)

    def _spawn_new_robot(self, base):
        free_cells = [cell for cell in self.world.map.neighbour_cells(base.coord) if cell.empty()]
        if len(free_cells) >= 1:
            new_robot = self._robot_factory.create_robot(base.team)
            coord = free_cells[0].coord
            self.world.add_robot(new_robot, coord)

    def _remove_dead_robots(self):
        dead_robots = [robot for robot in self.world.robots if robot.is_dead()]
        self.world.remove_robots(dead_robots)

    def _send_init(self):
        for l in self._listeners:
            l.send_init(self.teams, self.world.map.all_coords())

    def _send_update(self):
        for l in self._listeners:
            l.send_update(self._current_frame, self.world.robots, self.world.bases)

    def current_frame(self):
        return self._current_frame


