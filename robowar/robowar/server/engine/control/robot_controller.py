from robowar.server.engine.model.robot import Robot
from robowar.server.engine.model.base import Base

import copy

class ViewObject(object):
    
    def __init__(self, coord, team, distance):
        super().__init__()
        self.coord = coord
        self.team = team
        self.distance = distance
        

class RobotController(object):

    def __init__(self, robot, game_map):
        super().__init__()
        self._robot = robot
        self._map = game_map
        self.robots_in_view = []
        self.bases_in_view = []
        self.messages = []

    def reset(self):
        self._robot.reset()
        max_range = max(self._robot.view_range, self._robot.transmit_range)
        cells_in_range = self._map.cells_in_range(self._robot.coord, max_range)
        robots = self._get_objects(cells_in_range, Robot)
        bases = self._get_objects(cells_in_range, Base)
        self.robots_in_view = self._get_objects_in_view(robots)
        self.bases_in_view = self._get_objects_in_view(bases)
        self._receive_messages(robots)

    def _get_objects_in_view(self, objects):
        my_coord = self._robot.coord
        objects_in_view = [ViewObject(obj.coord, obj.team, obj.distance_to(my_coord)) for obj in objects]
        return [obj for obj in objects_in_view if obj.distance <= self._robot.view_range]

    def _get_objects(self, cells, obj_type):
        objects = [cell.get_object(obj_type) for cell in cells]
        return [obj for obj in objects if obj is not None]

    def _receive_messages(self, robots):
        transmitting_robots = [r for r in robots if r.can_transmit_to(self)]
        self.messages = [copy.deepcopy(r.transmission()) for r in transmitting_robots]

    def neighbour_coord(self, dir_index):
        return self._robot.coord.neighbour(dir_index)

    def can_move_in(self, coord):
        cell = self._map.cell(coord)
        return coord if cell and not cell.contains(Base) else None

    def can_move_in_direction(self, dir_index):
        return self.can_move_in(self.neighbour_coord(dir_index))

    def move_in(self, coord):
        self._robot.move_coord = coord if self.can_move_in(coord) else None

    def move_in_direction(self, dir_index):
        self.move_in(self.neighbour_coord(dir_index))

    def cancel_move(self):
        self._robot.cancel_move()

    def can_shoot(self):
        return self._robot.can_shoot()
        
    def can_shoot_at(self, coord):
        if not self.can_shoot():
            return False
        cell = self._map.cell(coord)
        return cell and (self._robot.distance_to(coord) <= self._robot.shoot_range)

    def shoot_at(self, coord):
        self._robot.shoot_coord = coord if self.can_shoot_at(coord) else None

    def cancel_shoot(self):
        self._robot.cancel_shoot()

    def set_transmit(self, transmit=True):
        self._robot.is_transmitting = transmit

    def is_transmitting(self):
        return self._robot.is_transmitting

    def transmission(self):
        return self._robot.transmission()

    def memory(self):
        return self._robot.memory()

    def coord(self):
        return self._robot.coord

    def team(self):
        return self._robot.team

    def is_enemy(self, view_object):
        return self._robot.is_enemy(view_object)

    def is_shooting(self):
        return self._robot.shoot_coord is not None

    def is_moving(self):
        return self._robot.move_coord is not None


        