from robowar.common.message.message_processor import MessageProcessor

class ServerMessageProcessor(MessageProcessor):
    """Processes messages on the server."""

    def __init__(self):
        super().__init__()

    def on_new_game(self, tag, data):
        print("tag: {0}, data: {1}".format(tag, data))
