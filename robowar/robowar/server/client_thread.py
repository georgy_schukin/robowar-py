import socket
import threading

from robowar.common.message.message_receiver import MessageReceiver
from robowar.common.message.message_processor import MessageProcessor

class ClientThread(threading.Thread):
    """Receives and process messages from client in separate thread."""

    def __init__(self, client_socket, message_processor=MessageProcessor()):
        super().__init__()
        self.socket = client_socket
        self.receiver = MessageReceiver(self.socket)
        self.processor = message_processor

    def run(self):
        try:
            while True:                
                message = self.receiver.receive(from_json=True)  # receive message as JSON object
                if message is None:
                    break  # null message - stop thread
                elif message:
                    self._process_message(message)
        except socket.error:
            print("Socket error!")
        except (ValueError, TypeError, OverflowError) as e:
            print(e)
        finally:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()    

    def _process_message(self, message):
        self.processor.process(message)
